#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include "./src/GF(256).h"
#include "./src/shamir.h"
#include <emscripten/bind.h>
#include <sstream>


using namespace GF256;
using namespace shamir;
using namespace emscripten;


extern "C" {

EMSCRIPTEN_KEEPALIVE
void secretsplit(char* inputstr, int N, int K)
{
	init(); //to initialise the library
	
	if (K>=N){
		std::cout << "Threshold parameter K must be strictly less than the total number of shards N" << std::endl;
      		return;
      	}
	
	
	std::string str=inputstr;
  	
  	std::stringstream result_stream;
  	
  	int d;
  	
  	scheme scheme1(N,K);
  	shares* scheme1_shares = scheme1.createShares(str);
  	std::string resultstr; 
	
	for(int i=0; i<N; i++) {
      		d=0;
      		
      		for(auto val:(*scheme1_shares)[i]) {
      			if (d==0){
      				result_stream << "Shard " << std::dec << (i+1) << ": " << N << "_" << K << "_" << val.x << "_" << std::hex<< val.y;
      				d=1;
      			}
      			else{
      				if (val.y<16){
      					result_stream << std::hex << "0" << val.y;
      					}
      				else
      				{
      					result_stream << std::hex << val.y;
      					}
	      		}
      		}
      		result_stream << std::endl;
      		resultstr = result_stream.str();

	};
	std::cout << "Generated shardes:" << std::endl << resultstr;
};

EMSCRIPTEN_KEEPALIVE
void secretcombining(char* inputstr)
{
	init();
	
	
	std::string str = inputstr;
	
	std::vector<std::string> v; //word for shardes
	
	// initializing variables
	int start, end;
	start = end = 0;

	// defining the delimitation character
	char dl = ' ';
	char dls = '_';
	size_t prev_found;
	size_t found;
	std::string inputsubword;
	std::string shard;
	
	
	//split on shardes by delimiter " "
	while ((start = str.find_first_not_of(dl, end))!= std::string::npos) {
		end = str.find(dl, start);
		v.push_back(str.substr(start, end - start));
	}
	
	//delete trash in last subword - DEPRICATED
	//found = v[v.size()-2].find("_",v[v.size()-2].find("_", v[v.size()-2].find("_")+1)+1);
	//inputsubword = v[v.size()-2].substr(found+1,std::string::npos);
	//v[v.size()-1] = v[v.size()-1].substr(0,found+1+inputsubword.length());
	

	//for (int i = 0; i < v.size(); i++) {
	//	std::cout << v[i] << std::endl;
	//}
	//std::cout << v.size() <<std::endl;
	//std::cout << (v[0].length() - 2) <<std::endl;

	int M = v.size();
	int N;
	int K;
	
	if ((found = v[0].find("_")) != std::string::npos)
	{
		N = stoi(v[0].substr(0,found));
		inputsubword = v[0].substr(found+1, std::string::npos);

	}
	
	//secornd digit is threshold
	if ((found = inputsubword.find("_")) != std::string::npos)
	{
		K = stoi(inputsubword.substr(0,found));
		shard = inputsubword.substr(found+1, std::string::npos);

	}
	
	//std::cout << N <<std::endl;
	//std::cout << K <<std::endl;
	
	scheme scheme1(N,K);
	
	point temp;
	byte x,y;
	std::string subword;
	
	
	shares* sharesM = new shares(M);
	for(int i=0; i<M; i++) {
		if ((found = v[i].find("_",v[i].find("_", v[i].find("_")+1)+1)) != std::string::npos)
		{
			prev_found = v[i].find("_",v[i].find("_")+1);
			x = (int) stoi(v[i].substr(prev_found+1,found));
			subword = v[i].substr(found+1, std::string::npos);

		}
		
		for (size_t j = 0; j < subword.size(); j += 2)
		{
		     std::string sub = subword.substr(j, 2);
		     temp.x=x;
		     temp.y= (int) stoi(sub, nullptr, 16);
		     (*sharesM)[i].push_back(temp);
		}
	}
	
	/*for(int i=0;i<M;i++) {
      		std::cout << "shares of pariticipant " << (i+1) << " => " << std::endl;
      		for(auto val:(*sharesM)[i]) {
          		std::cout << val.x << " " << val.y << std::endl;
      		}
      	std::cout <<  "_____________________________________________________" << std::endl;
	};*/
	
	std::string newSecret = scheme1.getSecret(sharesM);
	std::cout << "Secret restored using " << M << " shares: " << std::endl << newSecret << std::endl;
	
};
  
}
